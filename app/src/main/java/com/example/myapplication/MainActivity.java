package com.example.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calcIMC(View view)
    {
        TextView txtResult = (TextView) findViewById(R.id.textViewResult);
        EditText txtWeight = (EditText) findViewById(R.id.editTextWeight);
        EditText txtHeight = (EditText) findViewById(R.id.editTextHeight);
        double weight = Double.parseDouble(txtWeight.getText().toString());
        double height = Double.parseDouble(txtHeight.getText().toString());
        double result = weight / Math.pow(height, 2);
        txtResult.setText(String.format("%.1f", result));
    }
}
